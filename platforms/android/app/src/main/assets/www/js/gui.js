//Globale Ordner Liste
//https://www.iconfinder.com/icons/183362/cloud_sync_icon

var global_folder_list = [];
var global_profile_list = [];

var current_global_folder_obj;
var current_global_profile_obj;


function generate_profile_list(in_profile_list)
{
	"use strict";

	//Lang...
	try
	{
		document.getElementById("add_profile_btn").textContent = add_btn;
		document.getElementById("profil_head_text").textContent = sidebar_profil_text;
	}
	catch (error) {}
	
	//Löschen alter Benachrichtigungen
	cordova.plugins.notification.local.cancelAll(function(e){
		
		var profile_list_element = document.getElementById('profile_list');
		
		if(profile_list_element == null)
		{
			return;
		}
		profile_list_element.innerHTML = "";

		
		for (var counter = 0; counter < in_profile_list.length; counter++)
		{
				
			var profile_name = in_profile_list[counter].get_name() ;
			var profile_id =  in_profile_list[counter].get_id();
			var profile_time = in_profile_list[counter].get_last_time();
			var profile_status = in_profile_list[counter].get_sync_status();
			
			
			if( profile_list_element != null)
			{
				
				var current_state = '';
				
				if( profile_status == "-1")
				{
					current_state = '<div id="'+profile_id+'_btn" class="left" onclick="gui_start_sync('+profile_id+')" >';
					current_state = current_state + '	<img id="'+profile_id+'_img" class="list-item__thumbnail" src="img/start.png">';
				}
				else
				{
					current_state = '<div id="'+profile_id+'_btn" class="left" onclick="gui_stop_sync('+profile_id+')" >';
					current_state = current_state+  '	<img id="'+profile_id+'_img" class="list-item__thumbnail" src="img/stop.png">';
					profile_time = profile_status+"%";
				}
				
					  var profile_item = ons.createElement(
						 '<ons-list-item >' +
						 //'<div id="'+profile_id+'_btn" class="left" onclick="start_sync('+profile_id+')" >' +
						 current_state +
						 // '<ons-button modifier="quiet" >Start</ons-button>'+
						 //'	<img id="'+profile_id+'_img" class="list-item__thumbnail" src="img/start.png">'+
						 '</div>'+
						 '<div class="center" > '+
						 '	<span onclick="show_profile('+profile_id+')" class="list-item__title">'+profile_name+'</span><span id="'+profile_id+'_status" class="list-item__subtitle">'+profile_time+'</span>'+
						 '</div>'+
						 '<div class="right" onclick="delete_profile('+profile_id+')">'+
						 '	<ons-icon size="2x" style="color: grey;" icon="ion-ios-trash-outline, material:md-delete" class="ons-icon ion-ios-trash-outline ons-icon--ion"></ons-icon>'+
						 '</div>'+
						'</ons-list-item>'
					  );
					  
					profile_list_element.appendChild(profile_item);  
					
					//Falls vorhanden eine Benachrichtigung hinzufügen
					if(in_profile_list[counter].get_notif_type() == "daily")
					{
						var set_hour = parseInt(in_profile_list[counter].get_hour());
						var set_minute = parseInt(in_profile_list[counter].get_minute());
						try
						{
							cordova.plugins.notification.local.schedule(
							{
								id: ''+profile_id,
								title: 'LukeSync: '+profile_name,
								trigger: { every: { hour: set_hour, minute:set_minute } } //Jeden Tag um 11 uhr 4
							});	
						}
						catch(err) {}
					}
					
					if(in_profile_list[counter].get_notif_type() == "week")
					{
						var set_hour = parseInt(in_profile_list[counter].get_hour());
						var set_minute = parseInt(in_profile_list[counter].get_minute());
						var set_week_day = parseInt(in_profile_list[counter].get_weekday());
						try 
						{
							cordova.plugins.notification.local.schedule(
							{
								id: ''+profile_id,
								title: 'LukeSync: '+profile_name,
								trigger: { every: { weekday: set_week_day, hour: set_hour, minute:set_minute } } //Jeden Tag um 11 uhr 4
							});	
						}
						catch(err) {}
					}
				}	
	
		}
		
		cordova.plugins.notification.local.clearAll(function() {}, this);
		
		//Am ende abspeichern
		save_all_profiles();
	}, this);
}




function show_profile(in_id)
{
	
	var current_object = get_profile_object_by_id(in_id);

	//Testen, ob da gerade ein Sync läuft...
	if(current_object.get_sync_status() != "-1")
	{
		ons.notification.alert(stop_edit,{title:"LukeSync"});
		return;
	}
	
    current_global_profile_obj = current_object; //Das aktuelle Objekt wird entspechen gerade angezeigt
        
	document.querySelector('#myNavigator').pushPage('profile_edit.html').then(function()
	{
		
		document.getElementById("profile_name").value = current_object.get_name();
		
		for (var counter = 0; counter < global_folder_list.length; counter++)
		{
			if(global_folder_list[counter].get_name() != "")
			{
				var select_source_element = document.getElementById("select_source");
				var option = document.createElement("option");
				option.value = global_folder_list[counter].get_id();
				option.text = global_folder_list[counter].get_name();
				select_source_element.add(option);
				
				var select_target_element = document.getElementById("select_target");
				var option = document.createElement("option");
				option.value = global_folder_list[counter].get_id();
				option.text = global_folder_list[counter].get_name();
				select_source_element.add(option);
				select_target_element.add(option);
			}
		}
		
		if( current_object.get_source() != null)
		{
			document.getElementById('select_source').value = current_object.get_source().get_id();
		}
		else
		{
			document.getElementById('select_source').selectedIndex = -1;
		}
		
		if( current_object.get_dest() != null)
		{
			document.getElementById('select_target').value = current_object.get_dest().get_id();
		}
		else
		{
			document.getElementById('select_target').selectedIndex = -1;
		}
		
		document.getElementById("profil_src").textContent = profil_src + ":";
		document.getElementById("profil_target").textContent = profil_target + ":";
		document.getElementById("notify_type").textContent = notify_type + ":";

		document.getElementById("never").textContent = never;
		document.getElementById("daily").textContent = daily;
		document.getElementById("weekly").textContent = weekly;	
		
		document.getElementById("weekday").textContent = weekday;
		
		document.getElementById("profi_reminder_hour_text").textContent = profi_reminder_hour_text + ":";
		
		document.getElementById("min").textContent = min;
		document.getElementById("std").textContent = std;
		
		document.getElementById("monday").textContent = monday;
		document.getElementById("tuesday").textContent = tuesday;
		document.getElementById("wednesday").textContent = wednesday;
		document.getElementById("thursday").textContent = thursday;
		document.getElementById("friday").textContent = friday;
		document.getElementById("saturday").textContent = saturday;
		document.getElementById("sunday").textContent = sunday;
		
		document.getElementById("modify").textContent = modify;
		
		document.getElementById("profil_overwrite").textContent = profil_overwrite + ":";
		document.getElementById("overwrite_true").textContent = overwrite_true;
		document.getElementById("overwrite_false").textContent = overwrite_false;
		
		
		if( current_global_profile_obj.get_overwrite() == "true" || current_global_profile_obj.get_overwrite() == true)
		{
			document.getElementById("selectoverwrite").value = "true";
		}
		else
		{
			document.getElementById("selectoverwrite").value = "false";
		}
		
		update_profile_notify();
	});
}

function gui_start_sync(in_id)
{	
	document.getElementById(in_id+"_img").src = "img/stop.png";
	//Austauschen des Sync Icons...
	document.getElementById(in_id+"_btn").onclick = function (){gui_stop_sync(in_id);};
		
	function wait_for_all_finish()
	{
		if( get_profile_object_by_id(in_id).get_sync_status() == "-1" ) //Brauche nicht länger warten, wurde vom Benutzer beendet
		{
			return;
		}
		var should_start = 1;
		for (var counter = 0; counter < global_profile_list.length; counter++)
		{
			var status = global_profile_list[counter].get_sync_status() ;
		
			if(status != "-1")
			{
				if(status != "-2")
				{
					should_start = 0;
				}
			}
		}

		if(should_start == 0)
		{
			setTimeout(function(){wait_for_all_finish();  }, 100);	
		}
		else
		{
			get_profile_object_by_id(in_id).start_sync();
		}
	}
	
	get_profile_object_by_id(in_id).set_sync_status("-2"); //Gestartet aber warte bis alle fertig sind
	
	wait_for_all_finish();
}


function gui_stop_sync(in_id)
{	
	document.getElementById(in_id+"_img").src = "img/start.png";
	document.getElementById(in_id+"_btn").onclick = function (){gui_start_sync(in_id);};
	
	get_profile_object_by_id(in_id).stop_sync(); //Abbrechen
	

	var stopall = 1;
	for (var counter = 0; counter < global_profile_list.length; counter++)
	{
		var profile_status = global_profile_list[counter].get_sync_status();
		
		if( profile_status != "-1" )
		{
			stopall = 0;
		}
		
	}
	
	
	if( stopall == 1)
	{	//Background Modus wird beendet
		//cordova.plugins.backgroundMode.disable(); 
		window.plugins.insomnia.allowSleepAgain();
	}				
}





function add_profile()
{
	//At least one Folder must be present!
	if( global_folder_list.length < 1)
	{
		ons.notification.alert(folder_warn,{title:"LukeSync"});
		return;
	}
	
	var new_id = new Date().getTime();
	
	var new_profile = new SyncProfile(new_id,"Neues Profil");

	global_profile_list.push(new_profile);
	show_profile(new_id);
		
	generate_profile_list(global_profile_list); //Refresh the List
	
}


function generate_folder_list(in_folder_list)
{
	"use strict";

	//Lang...
	try
	{
	document.getElementById("add_folder_btn").textContent = add_btn;
	document.getElementById("folder_head_text").textContent = sidebar_folder_text;
	
	document.getElementById("local_folder_btn").textContent = local_folder_text;
	document.getElementById("ftp_folder_btn").textContent = ftp_folder_text;
	document.getElementById("close_btn").textContent = close_text;
	}
	catch (error) {}
	
	
	var folder_list_element = document.getElementById('folder_list');
	
	if(folder_list_element == null)
	{
		return;
	}
	folder_list_element.innerHTML = "";

	
	for (var counter = 0; counter < in_folder_list.length; counter++)
	{
			
		var folder_name = in_folder_list[counter].get_name()  ;
		var folder_path = in_folder_list[counter].get_path() ;
		var folder_id =   in_folder_list[counter].get_id();
	    var current_img = "";
		
		if(in_folder_list[counter].get_type() == "ftp") //Anderes Symbol bei einem anderem Typ, ansonsten ist es gleich
		{
			var current_img = "ftp.png";
		}
		else
		{
			var current_img = "folder.png";
		}
		
		if( folder_list_element != null)
		{
				  var folder_item = ons.createElement(
					 '<ons-list-item >' +
					 '<div class="left" onclick="show_folder('+folder_id+')" >' +
					 '	<img class="list-item__thumbnail" src="img/'+current_img+'">'+
					 '</div>'+
					 '<div class="center" > '+
					 '	<span onclick="show_folder('+folder_id+')" class="list-item__title">'+folder_name+'</span><span class="list-item__subtitle">'+folder_path+'</span>'+
					 '</div>'+
					 '<div class="right" onclick="delete_folder('+folder_id+')">'+
					 '	<ons-icon size="2x" style="color: grey;" icon="ion-ios-trash-outline, material:md-delete" class="ons-icon ion-ios-trash-outline ons-icon--ion"></ons-icon>'+
					 '</div>'+
					'</ons-list-item>'
				  );
				  
				folder_list_element.appendChild(folder_item);  
		}
	
	}
	//Am ende abspeichern
	save_all_folders();
}



//Liefert das passende Profil object zu einer id
function get_profile_object_by_id(in_id)
{
	"use strict";
	for (var counter = 0; counter < global_profile_list.length; counter++)
	{
		if(global_profile_list[counter].get_id() == in_id)
		{
			return global_profile_list[counter];
		}
	}
	return null;
}

//Liefert das passende folder object zu einer id
function get_folder_object_by_id(in_id)
{
	"use strict";
	for (var counter = 0; counter < global_folder_list.length; counter++)
	{
		if(global_folder_list[counter].get_id() == in_id)
		{
			return global_folder_list[counter];
		}
	}
	return null;
}

//Der aktuelle Profilname wurde angepasst
function update_profil_name()
{
	"use strict";
	var new_name = document.getElementById("profile_name").value;
	current_global_profile_obj.set_name(new_name);	
}


//Eingabefeld Benutzer FTP geändert
function update_ftp_name()
{
	"use strict";
	var new_name = document.getElementById("ftp_folder_name").value;
	current_global_folder_obj.set_name(new_name);	
}

//Eingabefeld Host FTP geändert
function update_ftp_host()
{
	"use strict";
	var new_host = document.getElementById("ftp_host").value;
	current_global_folder_obj.set_host(new_host);
}

//Eingabefeld User FTP geändert
function update_ftp_user()
{
	"use strict";
	var new_user = document.getElementById("ftp_user").value;
	current_global_folder_obj.set_user(new_user);	
}

//Eingabefeld Passwort FTP geändert
function update_ftp_pw()
{
	"use strict";
	var new_pw = document.getElementById("ftp_pw").value;
	current_global_folder_obj.set_pw(new_pw);	
}


//Eingabefeld Name Lokaler Odner geändert
function update_local_name()
{
	"use strict";
	var new_name = document.getElementById("local_folder_name").value;
	current_global_folder_obj.set_name(new_name);	
}




//Zeigt ein FolderProfil an
function show_folder(in_id)
{
	"use strict";
    var current_object = get_folder_object_by_id(in_id);

    current_global_folder_obj = current_object; //Das aktuelle Objekt wird entspechen gerade angezeigt
    
    if( current_object.get_type() == "ftp")
	{
		document.querySelector('#myNavigator').pushPage('folder_edit_ftp.html').then(function()
		{
			document.getElementById("ftp_folder_name").value = current_object.get_name();
			document.getElementById("ftp_host").value = current_object.get_host();
			document.getElementById("ftp_user").value = current_object.get_user();
			document.getElementById("ftp_pw").value = current_object.get_pw();
			document.getElementById("ftp_path").value = current_object.get_path();
			
			document.getElementById("folder_edit_ftp_head").textContent = modify;
			document.getElementById("ftp_folder_text").textContent= ftp_folder_text;
			
			document.getElementById("ftp_host").setAttribute('placeholder', ftp_host_placeholder);
			document.getElementById("ftp_user").setAttribute('placeholder', ftp_user_placeholder);
			document.getElementById("ftp_pw").setAttribute('placeholder', ftp_pw_placeholder);
			document.getElementById("ftp_path").setAttribute('placeholder', ftp_path_placeholder);
		});
    }
    
    if( current_object.get_type() == "local")
	{
		document.querySelector('#myNavigator').pushPage('folder_edit_local.html').then(function()
		{
			document.getElementById("local_folder_name").value = current_object.get_name();
			document.getElementById("local_path").value = current_object.get_path();
			
			document.getElementById("folder_edit_local_head").textContent = modify;
			
			document.getElementById("lokal_folder_text").textContent = lokal_folder_text;
			document.getElementById("lokal_name_text").textContent = lokal_name_text;
			
			document.getElementById("local_folder_name").setAttribute('placeholder', lokal_name_text);
			document.getElementById("local_path").setAttribute('placeholder', lokal_path + "...");
			
		});
    } 
}

//Bei zurück die Folderliste anpassen
function back_update_folder_list()
{
	generate_folder_list(global_folder_list);
}

//Bei zurück aus der Profilliste
function back_update_profile_list()
{
	generate_profile_list(global_profile_list);
}



//Ein OrdnerProfil löschen - erst bestätigen lassen!
function delete_folder(in_id)
{
	"use strict";
	ons.notification.confirm(confirm,{title:title_del} ).then(function(input) 
	{
		if(input)
		{
			var tmp_array = [];
			for (var counter = 0; counter < global_folder_list.length; counter++)
			{
		
				if(global_folder_list[counter].get_id() != in_id)
				{
					tmp_array.push(global_folder_list[counter]);
				}
			}
			
			global_folder_list = tmp_array;
			generate_folder_list(global_folder_list); //Aktualisieren
		}
	});
}


//Ein SyncProfil löschen - erst bestätigen lassen!
function delete_profile(in_id)
{
	"use strict";
	
	//Testen, ob da gerade ein Sync läuft...
	var current_object = get_profile_object_by_id(in_id);
	
	if(current_object.get_sync_status() != "-1")
	{
		ons.notification.alert(stop_del, {title:"LukeSync"});
		return;
	}
	
	ons.notification.confirm(confirm,{title:title_del} ).then(function(input) 
	{
		if(input)
		{
			var tmp_array = [];
			for (var counter = 0; counter < global_profile_list.length; counter++)
			{
		
				if(global_profile_list[counter].get_id() != in_id)
				{
					tmp_array.push(global_profile_list[counter]);
				}
			}
			
			global_profile_list = tmp_array;
			generate_profile_list(global_profile_list); //Aktualisieren
		}
	});
}


//Auswählen eines FTP Paths (Anzeigen der Ordner)
function select_ftp_path()
{    
	"use strict";
    document.querySelector('#myNavigator').pushPage('ftp_list_folders.html');
	
	list_ftp(current_global_folder_obj.get_path());
}

//Den aktuell angezeigen FTP Ordner wählen
function chose_current_ftp_folder()
{
	"use strict";
	var path_tmp = document.getElementById("current_ftp_path").textContent;
	
	path_tmp = path_tmp.replace("Pfad: ", "");
	path_tmp = path_tmp.trim();
	
	if( path_tmp == "Keine Verbindung!")
	{
		path_tmp = "/";
	}
	
	current_global_folder_obj.set_path(path_tmp);
	
	document.querySelector('#myNavigator').popPage().then(function()
	{
		document.getElementById("ftp_path").value = current_global_folder_obj.get_path();
	});
	
}


//Auswählen eines lokalen Ordners
function select_local_path()
{
	"use strict";
	add_file_path();
}


//Laden Funktion für das Side Panle - überschrieben
window.fn.load = function(page)
{
	"use strict";
	var content = document.getElementById('content');
	var menu = document.getElementById('menu');
	content.load(page)
	.then(function() 
	{
		if(page == "folders.html") //Wenn die Ordner angezeigt werden sollen
		{	
			generate_folder_list(global_folder_list);
		}
		
		if(page == "profiles.html") //Wenn die Ordner angezeigt werden sollen
		{	
			generate_profile_list(global_profile_list);
		}
		
		if(page == "about.html") //Wenn die Ordner angezeigt werden sollen
		{	
			document.getElementById("autor").textContent = autor_text;
		}
		
		if(page == "settings.html") //Bei der Darstellung der Einstellungen
		{	
			document.getElementById("selectlang").value = current_lang;
			document.getElementById("settings_head_text").textContent = settings_head_text;
			document.getElementById("settings_head").textContent = settings_head_text;
		}
		
		menu.close();
	});

}; 

//Ein neues FTP Profil anlegen
function add_ftp()
{
	"use strict";
	hidePopover();
	var new_id = new Date().getTime();
	
	var new_ftp_profile = new FTP_Profile(new_id,"Neu" , "192.168.1.1" , "User","Pass","/" )

	global_folder_list.push(new_ftp_profile);
	show_folder(new_id)		
}

//Ein neues lokales Profil anlegen
function add_local()
{
	"use strict";
	hidePopover();
	var new_id = new Date().getTime();
	
	var new_local_profile = new LocalPath(new_id,"Neu" ,"file:///storage/emulated/0/" )
	
	global_folder_list.push(new_local_profile);
	show_folder(new_id)
}

//Anzeigen des Popups für das hinzufügen
function showPopover(target)
{
	"use strict";
	document.getElementById('popover').show(target);
}

//Schließen des Popups für das hinzufügen
function hidePopover()
{
	"use strict";
	document.getElementById('popover').hide();
}



function update_profil_hour()
{
	"use strict";
	var new_var = document.getElementById('profi_reminder_hour').value;
	current_global_profile_obj.set_hour(new_var);
	
	var val_hour = current_global_profile_obj.get_hour();
	var val_min = current_global_profile_obj.get_minute();
	if(val_hour < 10){ val_hour= "0"+val_hour;}
	if(val_min < 10){ val_min = "0"+val_min;}
	document.getElementById("profi_reminder_hour_text").textContent = profi_reminder_hour_text + ": "+val_hour+":"+val_min;	
}


function update_profil_minute()
{
	"use strict";
	var new_var = document.getElementById('profi_reminder_minute').value;
	current_global_profile_obj.set_minute(new_var);
	
	var val_hour = current_global_profile_obj.get_hour();
	var val_min = current_global_profile_obj.get_minute();
	if(val_hour < 10){ val_hour= "0"+val_hour;}
	if(val_min < 10){ val_min = "0"+val_min;}
	document.getElementById("profi_reminder_hour_text").textContent = profi_reminder_hour_text +": "+val_hour+":"+val_min;	
}



function change_profil_type(in_c)
{
	"use strict";
	current_global_profile_obj.set_notif_type(in_c);
	update_profile_notify();
}


	
	
function update_profile_notify()
{
	"use strict";
	if( current_global_profile_obj.get_notif_type() == "none")
	{
		document.getElementById("profil_day").style.display = "none";
		document.getElementById("profil_time").style.display = "none";
		document.getElementById("type_none").checked = true;
	}

	if( current_global_profile_obj.get_notif_type() == "daily")
	{
		document.getElementById("profil_day").style.display = "none";
		document.getElementById("profil_time").style.display = "block";
		document.getElementById("type_daily").checked = true;		
	}
		
	if( current_global_profile_obj.get_notif_type() == "week")
	{
		document.getElementById("profil_day").style.display = "block";
		document.getElementById("profil_time").style.display = "block";	
		document.getElementById("type_week").checked = true;
		
		var current_week_day = current_global_profile_obj.get_weekday();
		document.getElementById("selectweekday").value = current_week_day;
	}
	
	if( current_global_profile_obj.get_notif_type() == "week" || current_global_profile_obj.get_notif_type() == "daily")
	{
		var val_hour = current_global_profile_obj.get_hour();
		var val_min = current_global_profile_obj.get_minute();
		
		document.getElementById("profi_reminder_hour").value = val_hour;
		document.getElementById("profi_reminder_minute").value = val_min;
	
		if(val_hour < 10){ val_hour= "0"+val_hour;}
		if(val_min < 10){ val_min = "0"+val_min;}
	
		document.getElementById("profi_reminder_hour_text").textContent = profi_reminder_hour_text+": "+val_hour+":"+val_min;
	}
}



function selectweekday(in_c)
{
	var current_week_day = document.getElementById("selectweekday").value;
	current_global_profile_obj.set_weekday(current_week_day);
}

function selectoverwrite(in_c)
{
	var current_overwrite = document.getElementById("selectoverwrite").value;
	
	if( current_overwrite == "true")
	{
		current_global_profile_obj.set_overwrite(true);
	}
	else
	{
		current_global_profile_obj.set_overwrite(false);
	}
}

function selectlang(in_c)
{
	var current_lang = document.getElementById("selectlang").value;
	set_lang(current_lang);
	save_settings();
}



function set_source_profil()
{
	var current_source = document.getElementById("select_source").value;
	var current_source_obj = get_folder_object_by_id(current_source);
	
	current_global_profile_obj.set_source( current_source_obj );

}


function set_target_profil()
{
	var current_target = document.getElementById("select_target").value;
	
	var current_target_obj = get_folder_object_by_id(current_target);
	
	current_global_profile_obj.set_dest( current_target_obj );
}




