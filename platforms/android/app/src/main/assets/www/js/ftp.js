//Globale Vars, die hier benötigt werden
var wait_for_folder = 0;
var folder_calls = 1;

var last_folder_list = "";
var last_folder_list_el;

class FTP_Profile
{
	constructor(in_id,in_name,in_host,in_user,in_pw,in_path) 
	{
		"use strict";

		if (typeof in_id === "object") //Überladen
		{
			this.id = in_id.id;
			this.name = in_id.name;
			this.host = in_id.host;
			this.user = in_id.user;
			this.pw = in_id.pw;
			this.path = in_id.path;
			this.type = "ftp";
		}
		else
		{
			this.id = in_id;
			this.name = in_name;
			this.host = in_host;
			this.user = in_user;
			this.pw = in_pw;
			this.path = in_path;
			this.type = "ftp";
		}
	}
	
	
	set_name(in_name)
	{
		this.name = in_name;
	}
	
	set_host(in_host)
	{
		this.host = in_host;
	}
	
	set_port(in_port)
	{
		this.port = in_port;
	}

	set_user(in_user)
	{
		this.user = in_user;
	}
	
	set_pw(in_pw)
	{
		this.pw = in_pw;
	}		
	
	get_host()
	{
		return this.host;
	}
	
	get_user()
	{
		return this.user;
	}
	
	get_pw()
	{
		return this.pw;
	}
	
	get_name()
	{
		"use strict";
		return this.name;
	}
	
	get_type()
	{
		"use strict";
		return "ftp";
	}
	
	get_id()
	{
		"use strict";
		return this.id;
	}
	
	set_path(in_path)
	{
		"use strict";
		this.path = in_path;
	}
	
	get_path()
	{
		"use strict";
		return this.path;
	}


	//Downloader einer Datei in den Tmp Ordner
	toTMP( in_file )
	{
		"use strict";
		var in_folder = in_file.substring(0,in_file.lastIndexOf("/")+1);
			
		var in_file_t = "";
		in_file_t = this.path +"/"+ in_file;

		in_file_t =  in_file_t.replace("///", "/");
		in_file_t =  in_file_t.replace("//", "/");
		
		window.cordova.plugin.ftp.connect(this.host ,this.user,this.pw, function(ok)
		{
			//Der Name ist immer tmp
			window.cordova.plugin.ftp.download("/data/data/luke.sync/cache/tmp",in_file_t, function(percent)
			{
				if (percent == 1)
				{
					var to = "/data/data/luke.sync/cache/"+in_file;
					to = to.replace("///", "/");
					to = to.replace("//", "/");
					to = "file://" + to;
					//Kopieren für den richtigen Namen
					copyFile("file:///data/data/luke.sync/cache/tmp",to);
				} 
			}, function(error)
			{	//Auch einem Fehler abschließen
				global_sub_copy = 1;
			});
			
		});
		
	}
	
	
	
	//Auflisten aller Dateien
	list_files()
	{
		"use strict";
		global_list = [];
		
		list_folder_rec_ftp(this.path,"/" ,this.host,this.user,this.pw);
		
		var rr_call = 0; //Zählt die rekursiven Aufrufe
		var global_list_tmp_ftp = []; //Speicher die gefundenen Dateien
		
		
		function list_folder_rec_ftp(in_path,sub, in_host,in_user,in_pw)
		{
		
			window.cordova.plugin.ftp.connect(in_host,in_user,in_pw, function(ok)
			{
				window.cordova.plugin.ftp.ls(in_path+"/"+sub, function(fileList) { onSuccessCallbacklsftp(fileList); },function(fail){ onFailCallbacklsftp();} );

				function onSuccessCallbacklsftp(data)
				{
					rr_call = rr_call - 1;

					for (var i in data)
					{
						if( data[i]["type"] == "1" ) //Es handelt sich um einen Ordner
						{
							//Rekursiver Aufruf
							var name = data[i]["name"];
							rr_call = rr_call + 1;
							list_folder_rec_ftp(in_path,sub + "/" + name, in_host,in_user,in_pw)
						}
						else
						{
							var name = data[i]["name"];
							name = "/"+sub + "/" +name;							
							name = name.replace("///", "/");
							name = name.replace("//", "/");
							global_list_tmp_ftp.push(name);
							
						}
					}
					
					if(rr_call < 0) //Rekursivität ist abgeschlossen
					{
						global_list = global_list_tmp_ftp;
						if(global_list == "") //Keine Dateien vorhanden
						{
							global_list = "nope";
						}
					}
				}

				function onFailCallbacklsftp()
				{	//Bei einem Fehler
					global_list = "nope";
				}
			
			});
		}
	}
	
	
	//Lädt eine Datei aus dem Tempordner hoch
	fromTMP( in_file )
	{		
		"use strict";
		var in_folder = in_file.substring(0,in_file.lastIndexOf("/")+1);
		
		in_folder = this.path +"/"+ in_folder;
		 
		if(in_folder == "")
		{
			in_folder = "/";
		}
		
		in_folder =  in_folder.replace("///", "/");
		in_folder =  in_folder.replace("//", "/");
		
		wait_for_folder = 0;	
		folder_calls = 1;
		
		create_folders_recursiv_ftp( in_folder,this.host ,this.user,this.pw);
		
		wait_for_folders(this.host ,this.user,this.pw , this.path);
		
		//Warten, bis alle Ordner auf dem FTP Server rekursiv erstellt sind
		function wait_for_folders(in_host,in_user,in_pw , in_path)
		{
			
			if( wait_for_folder == 0 ) //Ordner sind nochimmer nicht erstellt...warten...
			{
				setTimeout(function(){ wait_for_folders(in_host,in_user,in_pw , in_path );  }, wait_time);
			}
			else
			{
				
				window.cordova.plugin.ftp.connect(in_host,in_user,in_pw, function(ok)
				{
					 window.cordova.plugin.ftp.upload("/data/data/luke.sync/cache/"+in_file, in_path + in_file, function(percent)
					 {
						if (percent == 1) //Der Upload ist abgeschlossen (100%)
						{
							global_sub_copy = 1;
						} 
					}, 
					function(error)
					{	//Bei einem Fehlr beenden
						global_sub_copy = 1;
					});
				
				});
			}
		}
	}


	//Prüft, ob eine Datei auf dem FTP Server existiert
	check_file_exist(in_file)
	{
		"use strict";
		var in_folder = in_file.substring(0,in_file.lastIndexOf("/")+1);
		var in_name = in_file.substring(in_file.lastIndexOf("/")+1,in_file.length);
				
		in_folder = this.path +"/"+ in_folder;
		
		if(in_folder == "")
		{
			in_folder = "/";
		}
		
		in_folder =  in_folder.replace("///", "/");
		in_folder =  in_folder.replace("//", "/");
		
		in_name = in_name.replace("/", "");
		in_name = in_name.replace("//", "");

		if( in_folder == last_folder_list)
		{
											
			//for (var i in last_folder_list_el)
			for (var i = 0; i < last_folder_list_el.length; i++) 
			{	
				if( last_folder_list_el[i]["type"] != "1" )
				{
					var current_name = last_folder_list_el[i]["name"];
																		
					if( current_name == in_name)
					{
						//Datei existiert, kann abgebrochen werden
						back_exists = 1;
						return;
					}
				}
			}
			//Datei existiert wohl nicht
			back_exists = 0;
			return;
		}
		else
		{
			
			last_folder_list = in_folder;
			
			window.cordova.plugin.ftp.connect(this.host ,this.user,this.pw, function(ok)
			{	//Auflisten aller Dateien
				window.cordova.plugin.ftp.ls(in_folder, function(fileList) { onSuccessCallback_checkfile(fileList); },function(fail){ onFailCallback_checkfile(fail);} );

				function onSuccessCallback_checkfile(data)
				{
					last_folder_list_el = data;
					
					for (var i in data)
					{
						
						if( data[i]["type"] != "1" )
						{
							var current_name = data[i]["name"];
													
							if( current_name == in_name)
							{
								//Datei existiert, kann abgebrochen werden
								back_exists = 1;
								return;
							}
						}
					}
					//Datei existiert wohl nicht
					back_exists = 0;
				}
				
				function onFailCallback_checkfile(e)
				{
					 //Bei einem Fehler xistiert die Datei wohl nicht
					 back_exists = 0;
				}
									
			});	
		
		}	
	}
	
}



//Erstellt einen FTP Ordner rekursiv
function create_folders_recursiv_ftp(in_path,in_host,in_user,in_pw)
{
	"use strict";
	if( in_path=="/" )
	{
		wait_for_folder = 1;
		return;
	}
	
	if( in_path == "" )
	{
		wait_for_folder = 1;
		return;
	}
	
	folder_calls = folder_calls + 1;//Durchläuft alle Ordner
	
	var tmp_path = "";
	var sp_array = in_path.split('/');
	
	if( folder_calls == sp_array.length)
	{ 
		wait_for_folder = 1;
		return;
	}
	
	for(var i = 0; i < sp_array.length; i++)
	{
		if( i < folder_calls )
		{
			tmp_path = tmp_path + sp_array[i] + "/";
		}
		else
		{
			break;
		}
	}
	
	tmp_path = tmp_path.replace("//", "/");
	tmp_path = tmp_path.replace("///", "/");

	//TMP ist der Pfad, der jetzt versucht wird zu erstellen
	window.cordova.plugin.ftp.connect(in_host,in_user,in_pw, function(ok)
	{			
		window.cordova.plugin.ftp.mkdir( tmp_path, function(data)
		{
			if( folder_calls < sp_array.length ) 
			{ 
				create_folders_recursiv_ftp(in_path,in_host,in_user,in_pw); 
			}
			else
			{
				//Abgeschlossen
				wait_for_folder = 1;
				return;
			}
		} ,
		function(data)
		{			
			if( folder_calls < sp_array.length ) 
			{ 
				create_folders_recursiv_ftp(in_path,in_host,in_user,in_pw); 
			}
			else
			{
				//Abgeschlossen
				wait_for_folder = 1;
				return;
			}
		} );	
	
	});
}




/*
 * 
 * 
 * Ende der Klasse, jetzt allgemeine Funktionen...
 * 
 * 
 */
	


function add_ftp_path()
{
	"use strict";
	list_ftp("/");
}


function normalize(path)
{
    path = Array.prototype.join.apply(arguments,['/'])
    var sPath;
    while (sPath!==path) {
        sPath = n(path);
        path = n(sPath);
    }
    function n(s){return s.replace(/\/+/g,'/').replace(/\w+\/+\.\./g,'')}
    return path.replace(/^\//,'').replace(/\/$/,'');
}


//Listet ein FTP-Verzeichnis für eine Verbindung auf
function list_ftp(in_path)
{
	"use strict";

	var in_host = current_global_folder_obj.get_host();
	var in_user = current_global_folder_obj.get_user();
	var in_pw = current_global_folder_obj.get_pw();

	var back = "";

	in_path = normalize(in_path);
	in_path = "/" + in_path + "/";
	in_path = in_path.replace("//", "/");
	in_path = in_path.replace("///", "/");
	
	window.cordova.plugin.ftp.connect(in_host,in_user,in_pw, function(ok)
	{
		
		window.cordova.plugin.ftp.ls(in_path, function(fileList) { onSuccessCallbacklsftppre(fileList); },function(fail){ onFailCallbacklsftppre(fail);} );

		function onSuccessCallbacklsftppre(data)
		{
			document.getElementById('current_ftp_path').textContent = "Pfad: " + in_path;
			
			var folder_list_element = document.getElementById('folder_list_ftp');
			folder_list_element.innerHTML = "";
			
			if( in_path != "/")
			{
				var name = "..";
				var folder_item = ons.createElement(
							 '<ons-list-item >' +
							 '<div class="left" onclick="list_ftp(\''+in_path+'/'+name+'\')" >' +
							 '	<img class="list-item__thumbnail" src="img/folder.png">'+
							 '</div>'+
							 '<div class="center" > '+
							 '	<span onclick="list_ftp(\''+in_path+'/'+name+'\')" >'+name+'</span>'+
							 '</div>'+
							'</ons-list-item>'
				);
						  
				folder_list_element.appendChild(folder_item);  
			}
			
			var counter = 0;				
			for (var i in data)
			{
				if( data[i]["type"] == "1" ) //Es handelt sich um einen Ordner
				{
					counter = counter + 1;
					var name = data[i]["name"];
					var folder_item = ons.createElement(
						 '<ons-list-item >' +
						 '<div class="left" onclick="list_ftp(\''+in_path+'/'+name+'\')" >' +
						 '	<img class="list-item__thumbnail" src="img/folder.png">'+
						 '</div>'+
						 '<div class="center" > '+
						 '	<span onclick="list_ftp(\''+in_path+'/'+name+'\')" >'+name+'</span>'+
						 '</div>'+
						'</ons-list-item>'
					  );
					  
					folder_list_element.appendChild(folder_item);  
				} 
			}
			
			if(counter == 0)
			{
				var folder_item = ons.createElement(
					'<ons-list-item >' +
					'Keine Unterordner vorhanden!'+
					'</ons-list-item>'
				);
					  
				folder_list_element.appendChild(folder_item);  
			}
			
		}
		
		function onFailCallbacklsftppre(data)
		{	
			//Bei einem Fehler
		}
		
	});

		

}
