//Globale Vars, die hier benötigt werden
var global_sub_copy = 0;
var newFileUri_folder_tmp = "";


class LocalPath
{

	constructor(in_id,in_name,in_path) 
	{
		"use strict";
		if (typeof in_id === "object") //Überladen
		{
			this.id = in_id.id;
			this.name = in_id.name;
			this.path = in_id.path;
			this.type = "local";
		}
		else
		{
			this.id = in_id;
			this.name = in_name;
			this.path = in_path;
			this.type = "local";
		}
	}
	
  
	set_path(in_path)
	{
		"use strict";
		this.path = in_path;
	}
	
	get_path()
	{
		"use strict";
		return this.path;
	}
	
	get_type()
	{
		"use strict";
		return this.type;
	}
	
	get_id()
	{
		"use strict";
		return this.id;
	}
	
	get_name()
	{
		"use strict";
		return this.name;
	}
	
	set_name(in_name)
	{
		"use strict";
		this.name = in_name;
	}	
	
	list_files()
	{
		"use strict";
		
		global_list = [];
			
		list_folder_rec_files(this.path,"/");
		
		var rr_call = 0; //Für rekursiven Call
		var global_list_tmp = [];
		
		function list_folder_rec_files(in_path,sub)
		{
			window.resolveLocalFileSystemURL(in_path+"/"+sub, function (dirEntry) 
			{
				var directoryReader = dirEntry.createReader();
				directoryReader.readEntries(onSuccessCallbacklistfiles,onFailCallbacklistfiles);
			});

			function onSuccessCallbacklistfiles(data)
			{
				rr_call = rr_call - 1;

				for (var i in data)
				{
					if( data[i]["isDirectory"] == false) //Es handelt sich um eine Datei
					{
						var name = data[i]["name"];
						name = "/"+sub + "/" +name;
						
						name = name.replace("///", "/");
						name = name.replace("//", "/");
						global_list_tmp.push(name);
					}
					else
					{
						//Rekursivität...
						var name = data[i]["name"];
						rr_call = rr_call + 1;
						list_folder_rec_files(in_path ,sub + "/" + name);
					}
				}
				
				if(rr_call < 0)
				{
					global_list = global_list_tmp;
					if(global_list == "")
					{	//Keine Einträge vorhanden
						global_list = "nope";
					}
				}
			}

			function onFailCallbacklistfiles()
			{	//Bei einem Fehler werden keine Einträge zurück geliefert
				global_list = "nope";
			}
	
		}
	}
	
	//Prüft, ob eine Datei existiert
	check_file_exist(in_file)
	{
		"use strict";
		window.resolveLocalFileSystemURL(this.path + "/" + in_file, fileExists, fileDoesNotExist);
		
		function fileExists(fileEntry)
		{	//Globale Var -> Datei vorhanden
			back_exists = 1;
		}
		function fileDoesNotExist()
		{	//Globale Var -> Datei NICHT vorhanden
			back_exists = 0;
		}
	}
	
	//Kopiert eine Datei in den Temporären Ordner
	toTMP( in_file )
	{
		"use strict";
		copyFile(this.path  + "/" + in_file,"file:///data/data/luke.sync/cache/"+in_file);	
	}
	
	//Kopiet eine Datei aus dem Temporären Ordner ins Ziel
	fromTMP( in_file )
	{
		"use strict";
		copyFile("file:///data/data/luke.sync/cache/"+in_file , this.path  + "/" + in_file);
	}
	
}





/*
 * 
 * 
 * Ende der Klasse, jetzt allgemeine Funktionen...
 * 
 * 
 */
	
//Filesystem Kopierfunktion, Von -> nach
function copyFile(from, to )
{
	"use strict";
	var newFileUri_folder;
	var newFile_name;
     window.resolveLocalFileSystemURL(from,
          function(fileEntry)
          {
                newFileUri_folder  = to.substring(0,to.lastIndexOf("/")+1);
                
                newFile_name = to.substring(to.lastIndexOf("/")+1,to.length);
                window.resolveLocalFileSystemURL(newFileUri_folder,
					function(dirEntry)
					{
						fileEntry.copyTo(dirEntry, newFile_name, onSuccessCallbackcopy, errorCallbackcopy);
                    },errorCallback_folder);
          },errorCallbackfirst);
          
	function onSuccessCallbackcopy(data)
	{	//Kopieren war erfolgreich
		global_sub_copy = 1;
	}
	

	function errorCallback_folder(data)
	{
		if(newFileUri_folder_tmp=="")
		{
			newFileUri_folder_tmp = newFileUri_folder+"";
		}
		
	    var the_arr = newFileUri_folder_tmp.split('/');
		var folder = the_arr.pop(); //Letzen Teil entfernen
		newFileUri_folder_tmp = the_arr.join('/');
    
		window.resolveLocalFileSystemURL(newFileUri_folder_tmp,
		function(dirEntry)
		{
			newFileUri_folder_tmp = "";
			dirEntry.getDirectory(folder, {create: true}, function(){ copyFile(from, to )  ; });				
		},errorCallback_folder); //Erneut aufrufen, rekursiv um alle Ziel Ordner zu erstellen...				
	}
                  
    function errorCallbackfirst(data)
    {	//Trotz Fehler aktion beenden...
		global_sub_copy = 1; 
	}
	
	function errorCallbackcopy(data)
	{	//Trotz Fehler aktion beenden...
		global_sub_copy = 1; 
	}
	
}


//Löschen einer Datei, im Dateisystem
function rmFile(in_file)
{
	"use strict";
	var path = in_file.substring(0,in_file.lastIndexOf("/")+1);
	var filename = in_file.substring(in_file.lastIndexOf("/")+1,in_file.length);
	
	window.resolveLocalFileSystemURL(path, function(dir) 
	{
		dir.getFile(filename, {create:false}, function(fileEntry)
		{
			fileEntry.remove(function(){copy = 0;},function(error){copy = 0;},function(){copy = 0;});
		},
		function(error)
		{
			//Ist also ein Ordner...
			dir.getDirectory(filename, {}, 
			function(dirEntry) 
			{
				dirEntry.removeRecursively(function()
				{	//Erfolgreich, aktion abschließen
					copy = 0;
				},
				function()
				{	//Fehler, trotzdem aktion abschließen
					copy = 0;
				});
			});	
		}
		);
	});
}





//Für die GUI...
function add_file_path()
{
	"use strict";
	var startp = current_global_folder_obj.get_path();
	startp = startp.toString();
	startp = startp.replace("file:///", "/");
	startp = startp.replace("//", "/");
						
	window.OurCodeWorld.Filebrowser.folderPicker.single({
	startupPath:startp,	
    success: function(data){
			if(!data.length){
				// No folders selected
				return;
			}

		  current_global_folder_obj.set_path(data);
		  document.getElementById("local_path").value = current_global_folder_obj.get_path();
		},
		error: function(err){}
	})

}
