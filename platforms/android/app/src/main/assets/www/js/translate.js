var sidebar_settings_text = "";
var sidebar_profil_text = "";
var sidebar_folder_text = "";
var sidebar_about_text = "";

var add_btn = "";

var local_folder_text = "";
var ftp_folder_text = "";
var close_text = "";

var autor_text = "";

var folder_warn = "";

var stop_del = "";

var confirm = "";

var stop_edit = "";

var title_del = "";

var valid_src = "";

var valid_target = "";

var valid_src_target = "";

var profil_src = "";

var profil_target = "";

var notify_type = "";

var never = "";
var daily = "";
var weekly = "";

var weekday = "";

var profi_reminder_hour_text = "";

var min = "";
var std = "";

var monday = "";
var tuesday = "";
var wednesday = "";
var thursday = "";
var friday = "";
var saturday = "";
var sunday = "";
		
var modify = "";

var lokal_folder_text = "";
var lokal_name_text = "";
var lokal_path = "";

var ftp_folder_text = "";
var ftp_host_placeholder = "";
var ftp_user_placeholder = "";
var ftp_pw_placeholder = "";
var ftp_path_placeholder = "";

var settings_head_text = "";

var profil_overwrite = "";
var overwrite_true = "";
var overwrite_false = "";

//All element strings are changed in order to apply the current language
function set_lang(in_lang)
{
	current_lang = in_lang;
	
	if(in_lang == "eng")
	{
		//English
		sidebar_settings_text = "Settings";
		sidebar_profil_text = "Profils";
		sidebar_folder_text = "Folders";
		sidebar_about_text = "About";
		
		add_btn = "Add";
		
		local_folder_text = "Local Folder";
		ftp_folder_text = "FTP Folder";
		close_text = "Close";
		
		autor_text = "Created by Lukas Sökefeld";
		
		folder_warn = "At least one folder must exist!";
		
		stop_del = "Please stop to delete!";
		
		confirm = "Confirm";
		
		stop_edit = "Please stop to edit!";
		
		title_del = "Delete";
		
		valid_src = "Please indicate valid source!";
		
		valid_target = "Please enter a valid destination!";
		
		valid_src_target = "Source and destination must not be the same!";
		
		profil_src = "Source";
		
		profil_target = "Target";
		
		profil_overwrite = "Overwrite";
		
		notify_type = "Notify Type";
		
		never = "Never";
		daily = "Daily";
		weekly = "Weekly";
		
		weekday = "Weekday";
		
		profi_reminder_hour_text = "Time";
		
		min = "Minute";
		std = "Hour";
		
		monday = "Monday";
		tuesday = "Tuesday";
		wednesday = "Wednesday";
		thursday = "Thursday";
		friday = "Friday";
		saturday = "Saturday";
		sunday = "Sunday";
		
		modify = "Modify";
		
		lokal_folder_text = "Local Folder";
		lokal_name_text = "Name";
		lokal_path = "Path";
		ftp_folder_text = "FTP Folder";
		
		ftp_host_placeholder = "Ip/Address";
		ftp_user_placeholder = "User";
		ftp_pw_placeholder = "Password";
		ftp_path_placeholder = "Path...";
		settings_head_text = "Language";
		
		overwrite_true = "Always Overwrite";
		overwrite_false = "Keep (When same name)";
	}
	else
	{
		//German
		sidebar_settings_text = "Einstellungen";
		sidebar_profil_text = "Profile";
		sidebar_folder_text = "Ordner";
		sidebar_about_text = "Über";
		
		add_btn = "Hinzufügen";
		
		local_folder_text = "Lokaler Ordner";
		ftp_folder_text = "FTP Ordner";
		close_text = "Schließen";
		
		autor_text = "Erstellt von Lukas Sökefeld";
		
		folder_warn = "Mindestens ein Ordner muss vorhanden sein!";
		
		stop_del = "Bitte stoppen zum löschen!";
		
		confirm = "Bestätigen";
		
		stop_edit = "Bitte stoppen zum bearbeiten!";
		
		title_del = "Löschen";
		
		valid_src = "Bitte gültige Quelle angeben!";
		
		valid_target = "Bitte gültiges Ziel angeben!";
		
		valid_src_target = "Quelle und Ziel dürfen nicht gleich sein!";
		
		profil_src = "Quelle";
		
		profil_target = "Ziel";
		
		profil_overwrite = "Überschreiben";
		
		notify_type = "Benachrichtigen Art";
		
		never = "Niemals";
		daily = "Täglich";
		weekly = "Wöchentlich";
		
		weekday = "Wochentag";
		
		profi_reminder_hour_text = "Uhrzeit";
		
		min = "Minute";
		std = "Stunde";
		
		monday = "Montag";
		tuesday = "Dienstag";
		wednesday = "Mittwoch";
		thursday = "Donnerstag";
		friday = "Freitag";
		saturday = "Samstag";
		sunday = "Sonntag";
		
		modify = "Anpassen";
		
		lokal_folder_text = "Lokale Ordner";
		lokal_name_text = "Name";
		lokal_path = "Pfad";
		
		ftp_folder_text = "FTP Ordner"
		ftp_host_placeholder = "Ip/Addresse";
		ftp_user_placeholder = "Benutzer";
		ftp_pw_placeholder = "Passwort";
		ftp_path_placeholder = "Pfad...";
		settings_head_text = "Sprache";
		
		overwrite_true = "Immer überschreiben";
		overwrite_false = "Behalten (Bei gleichem Namen)";
	}

	try
	{
		document.getElementById("side_bar_profiles").textContent = sidebar_profil_text;
		document.getElementById("side_bar_folders").textContent = sidebar_folder_text;
		document.getElementById("side_bar_settings").textContent = sidebar_settings_text;
		document.getElementById("side_bar_about").textContent = sidebar_about_text;
	}
	catch (error) {}
	
	try
	{
		document.getElementById("add_profile_btn").textContent = add_btn;
		document.getElementById("profil_head_text").textContent = sidebar_profil_text;
	}
	catch (error) {}
	
	try
	{
		document.getElementById("settings_head_text").textContent = settings_head_text;
		document.getElementById("settings_head").textContent = settings_head_text;
	}
	catch (error) {}
	
	load_ready = 1;
}

