# LukeSync

Simple Backup solution.
[Download](https://gitlab.com/LukeSoftware/lukesync/-/blob/master/LukeSync.apk)

#### Main features:
* FTP and Filesystem sync.
* Two-way sync
* Notification support
* Only copy or overwrite, no deletion!
* Multilingual (English and German)

#### Technical:
Requirements for building:
* Nodejs and npm
* cordova (installed via npm)
* Java and the android sdk
* OnsenUI: https://github.com/OnsenUI/OnsenUI
* Dayjs: https://github.com/iamkun/dayjs
