//Globale Vars, die hier benötigt werden
var copy = 0;
var global_list = "";
var back_exists = -1;
var wait_time = 20; //In ms

var global_list_percent = 0;

var current_lang = ""; //Global language variable

//Connection Sync Profile
class SyncProfile
{
	constructor(in_id,in_name) 
	{
		if (typeof in_id === "object") //Überladen
		{
			
			var src_id = -1;
			var dest_id = -1;
			try
			{
				var t = JSON.stringify(in_id.src_obj);
				var src_id = JSON.parse(t).id;
				var t = JSON.stringify(in_id.dest_obj);
				var dest_id = JSON.parse(t).id;
			}catch (e) {}
			
			this.id = in_id.id;
			this.name = in_id.name;
			this.notif_type = in_id.notif_type;
			this.hour = in_id.hour;
			this.minute = in_id.minute;
			this.status = in_id.status;
			this.week_day = in_id.week_day;
			this.last_time = in_id.last_time;
			this.src_obj  = get_folder_by_id( src_id );
			this.dest_obj = get_folder_by_id( dest_id );
			
			if( in_id.overwrite == true || in_id.overwrite == "true")
			{
				this.overwrite = true;
			}
			else
			{
				this.overwrite = false;
			}
		}
		else
		{
			this.id = in_id;
			this.name = in_name;
			this.notif_type = "none";
			this.hour = 0;
			this.minute = 0;
			this.status = "-1";
			this.last_time = "-";
			this.src_obj = null;
			this.dest_obj = null;
			this.overwrite = false;
		}
	}
  
	get_name()
	{
		"use strict";
		return this.name;
	}
	
	set_name(in_name)
	{
		"use strict";
		this.name = in_name;
	}
	
	get_id()
	{
		return this.id;
	}

	get_sync_status()
	{
		"use strict";
		return this.status;
	}
		
	set_sync_status(in_stat)
	{
		"use strict";
		this.status = in_stat;
	}
	
	stop_sync()
	{
		"use strict";
		//0 -> bin gestartet....
		//-1 -> bin fertig oder noch nicht gestartet...
		//-2 -> gestartet aber warte
		document.getElementById(this.id+'_status').textContent =  this.last_time;
		this.status = "-1";
	}
				
	set_source( in_src_obj )
	{
		"use strict";
		this.src_obj = in_src_obj;
	}
	
	get_source()
	{
		"use strict";
		return this.src_obj;
	}
	
	
	set_dest( in_dest_obj )
	{
		"use strict";
		this.dest_obj = in_dest_obj;
	}
	
	get_dest( in_dest_obj )
	{
		"use strict";
		return this.dest_obj;
	}
		
	
	set_notif_type( in_type )
	{
		this.notif_type = in_type;
	}
	
	
	get_notif_type( in_type )
	{
		return this.notif_type;
	}
		
	set_hour( in_hour )
	{
		this.hour = in_hour;
	}
	
	get_hour()
	{
		return this.hour;
	}
	
	set_minute( in_minute )
	{
		this.minute = in_minute;
	}
	
	get_minute()
	{
		return this.minute;
	}
	
	set_weekday(in_day)
	{
		this.week_day = in_day;
	}
		
	get_weekday()
	{
		return this.week_day;
	}
	
	set_overwrite(in_t)
	{
		this.overwrite = in_t;
	}
	
	get_overwrite()
	{
		return this.overwrite;
	}
	
	get_last_time()
	{
		return this.last_time;
	}
	//Startet die Synconisation
	start_sync()
	{
		"use strict";
		global_list = "";
		this.status = "0"; //Quasi 0 Prozent

		//Reset:
		last_folder_list = "";
		last_folder_list_el = null;

		if(this.src_obj == null)
		{
			ons.notification.alert(valid_src, {title:"LukeSync"});
			gui_stop_sync(this.id);
			return;
		}
		
		if(this.dest_obj == null)
		{
			ons.notification.alert(valid_target, {title:"LukeSync"});
			gui_stop_sync(this.id);
			return;
		}
		
		if( this.src_obj.id == this.dest_obj.id )
		{
			ons.notification.alert(valid_src_target, {title:"LukeSync"});
			gui_stop_sync(this.id);
			return;
		}
		
		//Eventuell was anderes
		//cordova.plugins.backgroundMode.enable();
		window.plugins.insomnia.keepAwake();
		
		document.getElementById(this.id + '_status').textContent = "0%";

		var all_files = this.src_obj.list_files();
		wait_for_list(this, this.src_obj,this.dest_obj);
	}
	
}



function continue_sync(profil,src,dest)
{
	"use strict";
	if(copy == 0)
	{
		if(global_list.length == 0)
		{
			global_list = "";
			rmFile("file:///data/data/luke.sync/cache/tmp"); //Aufräumen
			
			profil.last_time = ""+ dayjs().locale('de').format('DD.MM.YY - HH:mm'); 
			profil.status = "-1"; //Abgeschlossen!
			save_all_profiles();
			gui_stop_sync(profil.id);
		}
		else
		{
			if(profil.status =="-1")
			{
				//jetzt abbrechen!;
				gui_stop_sync(profil.id);
				return;
			} 
			copy = 1;
			var current_file = global_list.pop();
			//Hier die Prozente.. 
			var calc_count = parseInt( parseInt(global_list_percent) -parseInt(global_list.length)  ) ;
			
			var percent =  parseFloat( parseFloat( 1 - global_list.length / global_list_percent) * 100) ;
			percent = parseFloat( percent.toFixed(2) );
			
			document.getElementById(profil.id+'_status').textContent =  percent + "% ("+ calc_count +"/"+global_list_percent + ")";
			sync_file( current_file , src,dest , profil.overwrite ) ;
			setTimeout(function(){ continue_sync(profil,src,dest) }, wait_time);
		}
	}
	else
	{
		setTimeout(function(){ continue_sync(profil,src,dest) }, wait_time);
	}
}



function wait_for_list(profil,src,dest)
{
	"use strict";
	if( global_list == "" )
	{
		setTimeout(function(){ wait_for_list(profil,src,dest) }, wait_time);
	}
	else
	{
		if( global_list == "nope")
		{
			global_list = [];
		}
		global_list_percent = global_list.length;
		
		continue_sync(profil,src,dest);
	}
}




//Eigentliche Sync Funktion
function sync_file(in_file , in_src_obj, in_dest_obj , overwrite )
{
	"use strict";
	back_exists = -1;
	in_dest_obj.check_file_exist(in_file);
	

	function wait_for_check()
	{
		if(back_exists ==  -1 )
		{
			setTimeout(function(){wait_for_check();  }, wait_time);
		}
		else
		{
			//Nur wenn die Datei noch nicht existiert kopieren
			if(back_exists == 0 || overwrite == true)
			{
				//Erst in TMP...
				global_sub_copy = 0;
				in_src_obj.toTMP(in_file);
				
				function continue_from_tmp()
				{
					if(global_sub_copy == 0)
					{
						setTimeout(function(){continue_from_tmp();  }, wait_time);
					}
					else
					{
						global_sub_copy = 0;
						in_dest_obj.fromTMP(in_file);
						function rm_cache()
						{
							if(global_sub_copy == 0)
							{
								setTimeout(function(){rm_cache();  }, wait_time);
							}
							else
							{
								global_sub_copy = 0;
								rmFile("file:///data/data/luke.sync/cache/"+in_file);
							}
						}
						rm_cache();
					}
					
				}
				
				continue_from_tmp();
				
			}
			else
			{
				//Datei existiert schon...
				copy = 0;
			}
		}
	}
	wait_for_check();
	
}



function get_folder_by_id(in_id)
{
	"use strict";
	var back = null;
	
	if( in_id == -1)
	{
		return back;
	}
	for (var counter = 0; counter < global_folder_list.length; counter++)
	{
		var current_obj = global_folder_list[counter];
		if( current_obj.get_id() ==  in_id)
		{		
			back = current_obj;
			break;
		}		
	}
				
	return back;
}



function save_all_folders()
{
	"use strict";	
	var json_save_string = JSON.stringify(global_folder_list);

	try 
	{
		NativeStorage.setItem("folders",""+json_save_string,function(){},function(){});
	}
	catch (e){}	
}


function save_all_profiles()
{
	"use strict";	
	
	var json_save_string = JSON.stringify(global_profile_list);
	try 
	{
		NativeStorage.setItem("profiles",""+json_save_string,function(){},function(){});
	}
	catch (e){}	
}



function load_saved_folders()
{
	"use strict";	
	
	try 
	{
		NativeStorage.getItem("folders",
		function(ine)
		{
	
			var global_folder_list_tmp = JSON.parse(ine);
			global_folder_list = [];
			for (var counter = 0; counter < global_folder_list_tmp.length; counter++)
			{
		
				var current_obj = global_folder_list_tmp[counter];
				if( current_obj.type == "local")
				{
					current_obj = new LocalPath( current_obj );
				}
				else
				{
					current_obj = new FTP_Profile( current_obj );
				}
				
				global_folder_list.push( current_obj );
				
			}
	
			generate_folder_list(global_folder_list);
		},
		function()
		{
			global_folder_list = []	
			generate_folder_list(global_folder_list);
		});
	}
	catch (e){}	
}



function load_saved_profiles()
{
	"use strict";
	try 
	{
		NativeStorage.getItem("profiles",
		function(ine)
		{
			var global_profile_list_tmp = JSON.parse(ine);
			global_profile_list = [];
			
			for (var counter = 0; counter < global_profile_list_tmp.length; counter++)
			{
				var current_obj = global_profile_list_tmp[counter];
				current_obj = new SyncProfile( current_obj );
				global_profile_list.push( current_obj );	
			}
			generate_profile_list(global_profile_list);
		},
		function()
		{
			global_profile_list = []	
			generate_profile_list(global_profile_list);
		});
	}
	catch (e){}	
}




function load_settings()
{
	"use strict";
	try 
	{
		NativeStorage.getItem("settings",
		function(ine)
		{
			var lang_tmp = ine;
						
			if(lang_tmp == "ger")
			{
				set_lang("ger");
			}
			else
			{
				set_lang("eng");
			}
		},
		function()
		{
			//Guess Lang...
			if(navigator.language.indexOf("de") > -1)
			{
				set_lang("ger");
			}
			else
			{
				set_lang("eng");
			}			
		});
	}
	catch(e)
	{
		set_lang("eng");
	}	
}




function save_settings()
{
	"use strict";	
	try 
	{
		NativeStorage.setItem("settings",current_lang,function(){},function(){});
	}
	catch (e){}	
}





//logcat chromium:D SystemWebViewClient:D *:S
//
